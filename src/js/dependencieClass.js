function removeById(id) {
  var element = document.getElementById(id);
  return element.parentNode.removeChild(element);
}

function hasClass(element, name) {
  return new RegExp('(\\s|^)' + name + '(\\s|$)').test(element.className);
}

function addClass(element, name) {
  if (!hasClass(element, name)) {
    element.className += (element.className ? ' ' : '') + name;
  }
}

function removeClass(element, name) {
  if (hasClass(element, name)) {
    element.className = element.className.replace(new RegExp('(\\s|^)' + name + '(\\s|$)'), ' ').replace(/^\s+|\s+$/g, '');
  }
}