/**
 * Esta función se encarga de hacer la animación principal.
 * Se compone de 4 subfunciones declaradas dentro de la misma.
 */

function loopAnimationPacience() {
  var elementPatience = document.getElementById('patience');
  var elementPatienceSpan = document.getElementById('patienceSpan');
  var imgMonk = document.getElementById('img-monk');
  var monkShadow = document.getElementById('preload');
  var containerText = document.getElementById('containerText');

  function firstPart() {
    addClass(elementPatience, 'patience-animation-first-part');
    elementPatience.innerHTML = 'Patience!';
    elementPatienceSpan.innerHTML = '';
    setTimeout(function () {
      removeClass(imgMonk, 'monk-start');
      addClass(imgMonk, 'movie-monk');
      removeClass(monkShadow, 'monk-shadow-start');
      addClass(monkShadow, 'monk-shadow');
    }, 650);
  }

  function secondPart() {
    removeClass(elementPatience, 'patience-animation-first-part');
    addClass(elementPatience, 'patience-animation-second-part');
  }

  function thirdPart() {
    removeClass(elementPatience, 'patience-animation-second-part');
    addClass(elementPatience, 'patience-animation-third-part');
    addClass(elementPatienceSpan, 'patience-span-animation-third-part');
    elementPatience.innerHTML = 'Patience ';
    elementPatienceSpan.innerHTML = 'young padawan...';
  }

  function fourPart() {
    removeClass(elementPatience, 'patience-animation-third-part');
    removeClass(elementPatienceSpan, 'patience-span-animation-third-part');
    addClass(containerText, 'patience-animation-four-part');
    elementPatience.innerHTML = 'Patience ';
    elementPatienceSpan.innerHTML = 'young padawan...';
  }

  if (elementPatience) {
    removeClass(containerText, 'patience-animation-four-part');

    firstPart();
    setTimeout(secondPart, 900);
    setTimeout(thirdPart, 1100);
    setTimeout(fourPart, 1700);
    setTimeout(loopAnimationPacience, 2700);

  }
}

/**
 * Esta función se llama cuando termina de cargar la imagen, para mostrar el carousel.
 */
function showCarousel() {
  var preloadContainer = document.getElementById('preload-container');
  var carousel = document.getElementById('carousel');
  if (!hasClass(preloadContainer, 'is-preload-loading')) {
    removeById('preload-container');
    removeClass(carousel, 'hidden');
    addClass(carousel, 'show');
  } else {
    addClass(preloadContainer, 'is-preload-loaded');
  }
}

/**
* Asigna el background según el ancho de la ventana, se tomo el valor de 700px como punto de cambio.
*/
function assignBackground() {
  if (window.innerWidth < 700) {
    loadImage('image/background-mobile.jpg');
  } else {
    loadImage('image/background.jpg');
  }
}

/**
 * Descarga y muestra el porcentaje de descarga del background.jpg
 * Al finalizar coloca la imagen en base64 como background, y muestra el carousel
 */
function loadImage(imageURI) {

  function updateProgressBar(e) {
    if (e.lengthComputable) {
      document.getElementById('preloadCounter').innerHTML = Math.round(e.loaded / e.total * 100) + '%';
    }
  }
  function assignBackgroundCss() {
    var encodedSrc = base64encode(request.responseText);
    var css = '#carousel { background-image: url("data:image/jpeg;base64,' + encodedSrc + '"); }';
    var style = document.createElement('style');
    style.appendChild(document.createTextNode(css));
    document.head.appendChild(style);
  }

  function onloadEnd() {
    assignBackgroundCss();
    showCarousel();
  }

  var request = new XMLHttpRequest();
  request.onprogress = updateProgressBar;
  request.onload = onloadEnd;
  request.open('GET', imageURI, true);
  request.overrideMimeType('text/plain; charset=x-user-defined');
  request.send(null);
}

  
/**
 * Cuando se carga el DOM comienza la animación de al menos un ciclo de 2.7s
 * Si termino de cargar la imagen se elimina del DOM y se muestra el carousel.
 */
window.addEventListener('DOMContentLoaded', function () {
  loopAnimationPacience();
  setTimeout(function () {
    var preloadContainer = document.getElementById('preload-container');
    var carousel = document.getElementById('carousel');
    removeClass(preloadContainer, 'is-preload-loading');
    if (hasClass(preloadContainer, 'is-preload-loaded')) {
      removeById('preload-container');
      removeClass(carousel, 'hidden');
      addClass(carousel, 'show');
    }
  }, 2700);
});


/**
 *
 * No fue necesario colocar un window.addEventListener('resize, assignBackground)
 * porque la imagen visualmente es igual.
 */
window.addEventListener('DOMContentLoaded', assignBackground);