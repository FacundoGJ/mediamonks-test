var carouselStates = [
  {
    percentage: '0%',
    text: ''
  }, {
    percentage: '13.4%',
    text: '1'
  }, {
    percentage: '22.6%',
    text: '2'
  }, {
    percentage: '37.1%',
    text: '3'
  }, {
    percentage: '51.3%',
    text: '4'
  }, {
    percentage: '65.4%',
    text: '5'
  }, {
    percentage: '80.3%',
    text: '6'
  }, {
    percentage: '98%',
    text: '7'
  }, {
    percentage: '100%',
    text: '8'
  }, {
    percentage: '115.7%',
    text: ''
  }
];

/**
* Muestra activo el botón del navegador.
*/
function addClassActiveNav(){
  var items = document.getElementById('navCarousel').children;
  
  for (var i = 0; i < carouselStates.length; i += 1) {
    removeClass(items[i].children[0],'active');
  }
  addClass(items[getCarouselIndex()].children[0],'active');
}

/**
* Muestra o no los botones <  >
*/
function addClassShowControls(){
  var controlBack = document.getElementById('controlBack');
  var controlNext = document.getElementById('controlNext');
  if(getCarouselIndex() == 0){
    controlBack.style.display = 'none';
    controlNext.style.display = 'block';
  } else if(getCarouselIndex() == carouselStates.length -1){
    controlBack.style.display = 'block';
    controlNext.style.display = 'none';
  } else {
    controlBack.style.display = 'block';
    controlNext.style.display = 'block';
  }
}

/**
* Mueve background image y ejecuta las funciones para el cambio de estado.
*/
function moveBackgroundImage(potition) {
  hiddenContentCarousel();
  if (potition >= 0 && potition < carouselStates.length) {
    document.getElementById('carousel').style.backgroundPosition = carouselStates[potition].percentage;
  }
  addClassActiveNav();
  addClassShowControls();
  setTimeout(showContentCarousel, 1000); 
}

/**
* Dibuja el navegador.
*/
function renderBotons() { 
  document.getElementById('navCarousel').innerHTML ='';
  for (var index = 0; index < carouselStates.length; index++) {
    document.getElementById('navCarousel').innerHTML += '<div onClick="moveBackgroundImage(' + index + ')">  <span>' + carouselStates[index].text + '</span></div>';
  }
}

/**
* Retorna la posición del carousel.
*/
function getCarouselIndex() {
  var valueBackgroundPosition = document.getElementById('carousel').style.backgroundPosition.split(" ")[0];
  for (var i = 0; i < carouselStates.length; i += 1) {
    if (carouselStates[i]['percentage'] === valueBackgroundPosition) {
      return i;
    }
  }
  return -1;
}

/**
* Mueve para atrás y adelante
*/
function nextMoveBackgroundImage(){
  moveBackgroundImage(getCarouselIndex()+1);
}
function backMoveBackgroundImage(){
  moveBackgroundImage(getCarouselIndex()-1);
}

/**
* Oculta todos los textos.
*/
function hiddenContentCarousel(){
  var items = document.getElementById('content').children;
  for (var i = 0; i < items.length; i += 1) {
    removeClass(items[i],'active');
  }
}

/**
* Muestra los textos que corresponden
*/
function showContentCarousel(){
  var items = document.getElementById('content').children;
  addClass(items[getCarouselIndex()],'active');
}

window.addEventListener('DOMContentLoaded', function () {
  renderBotons();
  addClassActiveNav();
  showContentCarousel();
});
