# Test MediaMonks

## Versión productiva:

[www.dacu.com.ar/monks-test/_dist/](http://www.dacu.com.ar/monks-test/_dist/)

## Versión para debuggear js y css:

[www.dacu.com.ar/monks-test/_debug/](http://www.dacu.com.ar/monks-test/_debug/)


## Comandos por unica vez:
```
npm install
npm install gulp -g
```

## Comandos para release:
```
gulp release
```
## Comandos para `watch:Sass` y `watch:html` :
```
gulp
```