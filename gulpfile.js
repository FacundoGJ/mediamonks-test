var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var fileinclude = require('gulp-file-include');
var gulp = require('gulp');
var gutil = require('gulp-util');
var htmlmin = require('gulp-htmlmin');
var minify = require('gulp-minify');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var clean = require('gulp-clean');
var strip_comments = require('gulp-strip-json-comments');
/**
* 
* * * 'release'
* * *  Se encarga de realizar todas las tareas necesarias para que funcione el proyecto
* 
* 'listJs' en lista los js que se unifican y minifican, se usa también para que sean llamados desde el html.
* 'cleanDistAndDebug' y 'copyStaticFolder' resetean las carpetas '_debug' y '_dist'
* 'js' es el encargado de minificar y unificar el js.
* 'sass' procesa el sass, exporta el map para la versión '_debug', coloca el autoprefixer y limpia los comentarios.
* 'fileInclude' procesa las 2 versiones de html y sale minificado para '_dist'
* 'watch' Se encarga observar si hay cambios para procesar el sass
*/

var listJs = [
  'src/js/dependencieClass.js',
  'src/js/base64encode.js',
  'src/js/preload.js',
  'src/js/carousel.js'
];

gulp.task('cleanDistAndDebug', () => {
  return gulp
    .src(['_dist', '_debug'])
    .pipe(clean());
});

gulp.task('copyStaticFolder', () => {
  return gulp
    .src(['src/static/.*', 'src/static/**'])
    .pipe(gulp.dest('_debug'))
    .pipe(gulp.dest('_dist'));
});


gulp.task('js', () => {
  return gulp
    .src(listJs)
    .pipe(concat('app.js'))
    .pipe(minify({exclude: ['tasks']}))
    .pipe(gulp.dest('_dist/js'));
});

gulp.task('sass', () => {
  return gulp
    .src('src/sass/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass(
      {outputStyle: 'compressed'}
      ).on('error', sass.logError))
    .pipe(concat('app.min.css'))
    .pipe(strip_comments())
    .pipe(autoprefixer())
    .pipe(gulp.dest('_dist/css'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('_debug/css'))
});

gulp.task('fileInclude', () => {
  gulp
    .src('src/views/index.html')
    .pipe(fileinclude({
      context: {
        folder: '../',
        listJs: listJs
      }
    }))
    .pipe(gulp.dest('_debug'));

  gulp
    .src('src/views/index.html')
    .pipe(fileinclude({
      context: {
        folder: '',
        listJs: ['js/app-min.js']
      }
    }))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('_dist'))
});

var watchLogger = (event) => {
  gutil.log('[' + event.type + '] ' + event.path);
};

gulp.task('watch', ['release'], () => {

  var wSASS = gulp.watch('src/sass/**/*.scss', ['sass']);
  wSASS.on('change add unlink', watchLogger);

  var hHtml = gulp.watch(['src/views/**'], ['fileInclude']);
  hHtml.on('change add unlink', watchLogger);

  var hHtml = gulp.watch([
    'src/static/.*', 'src/static/**'
  ], ['copyStaticFolder']);
  hHtml.on('change add unlink', watchLogger);
});

gulp.task('release', (cb) => {
  runSequence('cleanDistAndDebug', 'copyStaticFolder', 'fileInclude', 'sass', 'js', cb);
});

gulp.task('default', ['watch']);